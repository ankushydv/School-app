import React from 'react'
import { CFooter } from '@coreui/react'

const TheFooter = () => {
  return (
    <CFooter fixed={false}>
      <div>
        <p >School App</p>
        <span className="ml-1">&copy; 2021 QuyTech.</span>
      </div>
      <div className="mfs-auto">
        <span className="mr-1">Powered by</span>
        <p >CoreUI for React</p>
      </div>
    </CFooter>
  )
}

export default React.memo(TheFooter)
