import React from 'react'
import {
  CDropdown,
  CDropdownItem,
  CDropdownMenu,
  CDropdownToggle,
  CImg
} from '@coreui/react'
import CIcon from '@coreui/icons-react'
import {Link} from "react-router-dom"
import {useHistory} from "react-router-dom"
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faCaretDown } from '@fortawesome/free-solid-svg-icons'

const TheHeaderDropdown = () => {
  const history = useHistory();
  return (
    <CDropdown
      inNav
      className="c-header-nav-items "
      direction="down"
    >
      <CDropdownToggle className="c-header-nav-link" caret={false}>
        <div className="c-avatar">
          <CImg
            src={'avatars/6.jpg'}
            className="c-avatar-img"
            alt="admin@bootstrapmaster.com"
            style={{width:'42px', height:'42px'}}
          />
          <FontAwesomeIcon icon={faCaretDown} style={{ marginLeft:'10px', color:'black', fontSize:'15px'}}/>

        </div>
        <div style={{marginLeft:'14px'}} >
        <p style={{margin:'0px', fontWeight:"700"}}>Alina Mclourd</p> 
         <p style={{margin:'0px',color:'lightgrey'}}>Senior Manager</p>
        </div>

      </CDropdownToggle>
     
      <CDropdownMenu className="pt-0" placement="bottom-end">
       
        <CDropdownItem>
        
          <CIcon name="cil-user" className="mfe-2" />
          <Link to="/myprofile" style={{textDecoration:'none',color:'rgba(0, 0, 21, 0.7)'}}>
          My Profile
          </Link>
        </CDropdownItem>
       
        <CDropdownItem onClick={() => history.push("/changepassword")}>
          <CIcon name="cil-settings" className="mfe-2" />
           Change Password
        </CDropdownItem>
        <CDropdownItem divider />
        <CDropdownItem>
          <CIcon name="cil-lock-locked" className="mfe-2" />
          <Link to="/login" style={{textDecoration:'none'}}>Log Out</Link>
        </CDropdownItem>
      </CDropdownMenu>
    </CDropdown>
  )
}

export default TheHeaderDropdown
