import React from 'react'
import { useSelector, useDispatch } from 'react-redux'
import {
  CHeader,
  CToggler,
  CHeaderNav,

} from '@coreui/react'

// routes config
// import routes from '../routes'
// import logo from "./logo-lg.png";
import Hamburger from 'hamburger-react';

import {
  TheHeaderDropdown,
  TheHeaderDropdownMssg,
  TheHeaderDropdownNotif,
  TheHeaderDropdownTasks
} from './index'

import "./ThHeader.css"

const TheHeader = () => {
  const dispatch = useDispatch()
  const sidebarShow = useSelector(state => state.sidebarShow)

  const toggleSidebar = () => {
    const val = [true, 'responsive'].includes(sidebarShow) ? false : 'responsive'
    dispatch({ type: 'set', sidebarShow: val })
  }

  const toggleSidebarMobile = () => {
    const val = [false, 'responsive'].includes(sidebarShow) ? true : 'responsive'
    dispatch({ type: 'set', sidebarShow: val })
  }

  return (
    <CHeader withSubheader style={{ height: "63px", boxShadow: "0 0.46875rem 2.1875rem rgb(4 9 20 / 3%), 0 0.9375rem 1.40625rem rgb(4 9 20 / 3%), 0 0.25rem 0.53125rem rgb(4 9 20 / 5%), 0 0.125rem 0.1875rem rgb(4 9 20 / 3%)" }} >


      {/* <div className="logo-skul"><img src={logo} alt="logo" width="120px" height="50px" /></div> */}
      {/* <CToggler
        inHeader
        className="ml-1 d-md-down-none"
        onClick={toggleSidebar}
      /> */}
      <span onClick={toggleSidebar}
        style={{ marginTop: '0.6rem', marginLeft:'1em' }}
      >
        <Hamburger

          // className="ml-1  d-md-down-none"
          onClick={toggleSidebar}
          color="#3f6ad8"
          size={24}

        />

      </span>

      {/* mobile hamburger does'nt show */}

      {/* <CToggler
        inHeader
        className="mr-auto ml-1 d-lg-none"
        onClick={toggleSidebarMobile}
      /> */}
      <div className="heading">
        <h5>Senior Boys School Camroon
          </h5>
      </div>


      <CHeaderNav className="ml-auto">
        <TheHeaderDropdownNotif />
        <TheHeaderDropdownMssg />
        <TheHeaderDropdownTasks />
        <TheHeaderDropdown />
      </CHeaderNav>
    </CHeader>
  )
}

export default TheHeader
