import React from 'react'
import { useSelector, useDispatch } from 'react-redux'
import {
  CCreateElement,
  CSidebar,
  CSidebarBrand,
  CSidebarNav,
  CSidebarNavDivider,
  CSidebarNavTitle,
  CSidebarMinimizer,
  CSidebarNavDropdown,
  CSidebarNavItem,
} from '@coreui/react'

// import CIcon from '@coreui/icons-react'

// sidebar nav config
import navigation from './_nav'
// import logo from "./logo-lg.png";
import "./Sidebar.css";
import logo from "./logo-lg.png";



const TheSidebar = () => {
  const dispatch = useDispatch()
  const show = useSelector(state => !state.sidebarShow)

  return (
    <CSidebar
      show={show}
      onShowChange={(val) => dispatch({type: 'set', sidebarShow: val })}
      style={{backgroundColor:'white'}}
    >
      <CSidebarBrand className="d-md-down-none" to="/" style={{backgroundColor:'white'}}>
        {/* //barnd or Pic or LOgo */}
        <img src={logo} alt="brand" width="120px" height="50px" />
      </CSidebarBrand>
      <CSidebarNav >

        <CCreateElement
          items={navigation}
          components={{
            CSidebarNavDivider,
            CSidebarNavDropdown,
            CSidebarNavItem,
            CSidebarNavTitle
          }}
          className="link-sidebar"
        />
      </CSidebarNav>
      {/* <CSidebarMinimizer className="c-d-md-down-none"/> */}
    </CSidebar>
  )
}

export default React.memo(TheSidebar)
