import React from 'react'
import {
    CContainer,
    CRow,
    CCol,
    CCardBody,
    CCard,
    CButton,
    CSwitch
  } from '@coreui/react'

import UserPhoto from "../../assets/user-image/1.jpg";
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import {faEnvelope,faPhoneAlt,faClipboard, faStar, faBell} from "@fortawesome/free-solid-svg-icons"
import {useHistory} from "react-router-dom"

const MyProfile = () => {
    const history  = useHistory()
    const btnStyle ={
        backgroundColor:'#343a40',
        color:"#fff",
        borderColor:"#343a40",
        borderRadius:"14px",
        border:"1px solid transparent",
        fontSize:'0.8rem',
        padding:"0.355rem 1.5rem",
        margin:"10px 2px"

    }
    const imgStyle ={
        border:"2px solid white",
        marginTop:'22px',
        borderRadius:"8px"
    }
    const userDetails ={
        listStyleType:'none',
        margin:"20px 5px"
    }
   
    return (
        <CContainer>
           <CCardBody >
               <CCard style={{border:"1px solid #3ac47d"}}>
                   <CRow>
                       <CCol md="4" style={{backgroundColor:"#3ac47d", textAlign:'center', display:'flex',justifyContent:'center', alignItems:'center'}}>
                          {/* square image and green bg */}
                            <div >
                            <img alt="myPicture" src={UserPhoto} style={imgStyle}  width="100px" height="100px"/>

                                <h5 style={{color:"#fff",marginTop:'10px'}}>Alina Mclourd</h5>

                                <button style={btnStyle} className="ma-2">
                                    Edit
                                </button>
                            </div>
                       </CCol>
                       <CCol md="8">
                       <ul style={userDetails}>
                           <li>
                               <p>  <FontAwesomeIcon icon={faEnvelope} style={{marginRight:'10px'}}/>
                                   <strong>Email Address :</strong>alinamclourd@gmail.com</p> 
                               </li>
                           <li><p> 
                           <FontAwesomeIcon icon={faPhoneAlt} style={{marginRight:'10px'}}/>
                               <strong>Phone Number :</strong> +919898989898</p></li>
                           <li><p>
                           <FontAwesomeIcon icon={faClipboard} style={{marginRight:'10px'}}/>
                               <strong>Subject :</strong>Math, English, Science</p></li>
                           <li><p>
                           <FontAwesomeIcon icon={faStar    } style={{marginRight:'10px'}}/>
                               <strong>Grade Teacher : </strong>4.5 Grade</p></li>
                           <li style={{display:"flex",alignItems:'center'}} >
                               <p>
                               <FontAwesomeIcon icon={faBell} style={{marginRight:'10px'}}/> 
                                   <strong>Notification :</strong></p> 
                           <CSwitch className={'mx-5'} variant={'3d'} color={'primary'}  onChange={(e)=>console.log(e.target.checked)}/></li>
                       </ul>
                       <CButton
                        onClick={() => history.push("/changepassword")}
                        color="primary" 
                        style={{marginLeft:'40px', marginBottom:'20px', borderRadius:'20px',padding:'5px 18px', fontSize:'13px'}}>Change Password
                       </CButton>

                       </CCol>
                   </CRow>
               </CCard>
           </CCardBody>
        </CContainer>
    )
   
}

export default MyProfile
