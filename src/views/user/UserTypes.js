import React from "react";
import {
  CBadge,
  CCard,
  CCardBody,
  CCardHeader,
  CCol,
  CRow,
} from "@coreui/react";
import {Table} from "react-bootstrap"
import Excel from "../../assets/images/icons/excel.svg";
import Pdf from "../../assets/images/icons/pdf.svg";

const UserTypes = () => {
  return (
    <CRow>
      <CCardBody>
        <CCard>
            <div className="user-header"  style={{display:"flex", justifyContent:'space-between', alignItems:'center'}}>
            <CCardHeader style={{backgroundColor:'white', fontSize:'20px'}}>
              User Types
            </CCardHeader>
            <CCardHeader style={{backgroundColor:'white'}}>
              <img src={Excel} alt="excel" height="35px" className="mr-2"/>
              <img src={Pdf} alt="excel" height="35px" className="mr-2"/>
            </CCardHeader>
            </div>
          <CCol md="12">
            <Table striped bordered hover>
              <thead>
                <tr>
                  <th style={{color:"#3f6ad8"}}>S.No.</th>
                  <th style={{color:"#3f6ad8"}}>User Types</th>
                  <th style={{color:"#3f6ad8"}}>Status</th>
                  {/* <th>Username</th> */}
                </tr>
              </thead>
              <tbody>
                <tr>
                  <td>1</td>
                  <td>Students</td>
                  <td><span className="badge badge-success">Active</span></td>
                </tr>
                <tr>
                  <td>2</td>
                  <td>Parent</td>
                  <td><span className="badge badge-warning">Inactive</span></td>
                </tr>
                <tr>
                  <td>3</td>
                  <td >Teacher</td>
                  <td><span className="badge badge-success">Active</span></td>
                </tr>
                <tr>
                    <td>4</td>
                    <td>School Admin</td>
                    <td><span className="badge badge-warning">Inactive</span></td>
                </tr>
              </tbody>
            </Table>
          </CCol>
        </CCard>
      </CCardBody>
    </CRow>
  );
};

export default UserTypes;
