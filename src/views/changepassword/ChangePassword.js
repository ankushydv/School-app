import React from 'react';
import {
    CContainer,
    CCardBody,
    CCard,
    CInputGroup,
    CInputGroupPrepend,
    CInputGroupText,
    CInput,


} from '@coreui/react'
import CIcon from '@coreui/icons-react'

const ChangePassword = () => {
    return (
        // Lock Icon in input
        <CContainer  >
            <CCardBody style={{ display: 'flex', justifyContent: 'center' }}>
                <CCard style={{ width: "60%", border: "1px solid #3f6ad8" }}>
                    <div style={{ textAlign: 'center' }}>
                        <div className="py-3 px-5">
                            <h4 className="text-center mt-4">Change Password</h4>
                            <form>
                            <CInputGroup className="mb-3">
                                    <CInputGroupPrepend>
                                        <CInputGroupText>
                                            <CIcon name="cil-lock-locked" />
                                        </CInputGroupText>
                                    </CInputGroupPrepend>
                                    <CInput type="password" placeholder="Enter Current Password" autoComplete="new-password" />
                                </CInputGroup>
                                <CInputGroup className="mb-3">
                                    <CInputGroupPrepend>
                                        <CInputGroupText>
                                            <CIcon name="cil-lock-locked" />
                                        </CInputGroupText>
                                    </CInputGroupPrepend>
                                    <CInput type="password" placeholder="Enter New Password" autoComplete="new-password" />
                                </CInputGroup>
                                <CInputGroup className="mb-3">
                                    <CInputGroupPrepend>
                                        <CInputGroupText>
                                            <CIcon name="cil-lock-locked" />
                                        </CInputGroupText>
                                    </CInputGroupPrepend>
                                    <CInput type="password" placeholder="Enter New Password" autoComplete="new-password" />
                                </CInputGroup>
                            </form>
                            <button className="mb-2 mr-2 btn-pill btn btn-primary px-4 py-2 mt-2 btn-block" style={{ borderRadius: "20px" }}><h6 className="mt-1">Save Changes</h6></button>
                        </div>
                    </div>
                </CCard>
            </CCardBody>
        </CContainer>
    )
}

export default ChangePassword
