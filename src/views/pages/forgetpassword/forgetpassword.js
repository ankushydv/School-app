import React from 'react'
import { Link } from 'react-router-dom'
import {
  CButton,
  CCol,
  CInput,
  CFormGroup,
  CRow,
  CLabel
} from '@coreui/react'
import Slides from "../login/Slider"
import Logo from "../../loginimage/logo-n.png";


const Login = () => {
  return (
    <div >
      <CRow style={{backgroundColor:'white'}}>
        <CCol md="4">
          <Slides />
        </CCol>
        <CCol md="8" >
          <div className="fo" style={{ marginTop: "20%", marginLeft: "12%", backgroundColor:'#fff' }}>
            <img src={Logo} alt="logo" width="98px" height="150px" className="ml-4 mb-3" />
            <p style={{ fontSize: '30px', marginLeft: '10px', color: 'gray' }}>Forgot your Password?</p>
            <p style={{ fontSize: '20px', marginLeft: '10px', color: 'black' }}>Use the form below to recover it.</p>
            <CRow className="ml-2">
              
              <CCol md="10">
              
                <CFormGroup>
                  <CLabel htmlFor="name">Email</CLabel>
                  <CInput id="name" placeholder="Enter your Email" required />
                </CFormGroup>
              </CCol>
              
            </CRow>
          </div>
          <div style={{display:'flex', justifyContent:'space-between',marginLeft:'8%', padding:'1rem 5rem'}}>
           <Link to="/login" style={{textDecoration:'none',cursor:'pointer',}}> <p style={{color:'#3f6ad8'}}>Sign in existing account</p></Link>
            <CButton style={{backgroundColor:'#3f6ad8',color:'white'}}>Recover  Password</CButton>
        </div>
        </CCol>
        
      </CRow>
      
    </div>
  )
}

export default Login
