import React from 'react'
import { Link } from 'react-router-dom'
import {
  CButton,
  CCol,
  CInput,
  CFormGroup,
  CRow,
  CLabel
} from '@coreui/react'
import Slides from "./Slider"
import Logo from "../../loginimage/logo-n.png";
// import {useHistory} from "react-router-dom"

import "./login.css"

const Login = () => {
  // const history = useHistory();
  return (
      <CRow style={{backgroundColor:'white'}} >
        <CCol md="4" lg="4" className="slide-main">
          <Slides style={{height:'50%'}} />
        </CCol>
        <CCol md="12" lg="8" xs="12" className="login-main" style={{backgroundColor:'white'}}>
          <div className="fo" style={{ marginTop: "20%", marginLeft: "12%",backgroundColor:'#fff'}}>
            <img src={Logo} alt="logo" width="98px" height="150px" className="ml-4 mb-3" />
            <p style={{ fontSize: '20px', marginLeft: '10px', color: 'gray' }}>Please sign in to your account</p>
            <hr></hr>
            <CRow className="ml-2">
              
              <CCol md="5">
              
                <CFormGroup>
                  <CLabel htmlFor="name">Email</CLabel>
                  <CInput id="name" placeholder="Enter your Email" required />
                </CFormGroup>
              </CCol>
              <CCol md="5" >
                <CFormGroup>
                  <CLabel htmlFor="name">Password</CLabel>
                  <CInput id="password" type="current-password" placeholder="Enter your Password" required />
                </CFormGroup>
              </CCol>
              
            </CRow>
            <div style={{display:'flex', alignItems:'center',marginLeft:'30px'}}>
                <input type="checkbox"></input>
                <p style={{marginTop:'11px', padding:'5px'}}>Keep me logged in</p>
              </div>
            <hr></hr>
          </div>
          <div style={{marginLeft:'60%',display:'flex'}}>
          <Link to="/forgetpassword" style={{textDecoration:'none'}}><p className="mr-4 mt-2" style={{color:'#3f6ad8', }}>Recover Password</p></Link>
          <CButton color="primary" to="/" style={{padding:"10px",textAlign:'center'}}>Login to Dasboard</CButton>
          </div>

        </CCol>
        
      </CRow>
      
     
  )
}

export default Login
