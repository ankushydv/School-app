import React from 'react'
import { Carousel } from "react-bootstrap"
//images

import Slide1 from "../../loginimage/1.jpg";
import Slide2 from "../../loginimage/2.jpg"
import Slide3 from "../../loginimage/3.jpg"

// mask the image
import "./Slider.css"

const Slider = () => {
  return (
    <div style={{ width: '34.5vw' }} >
      <Carousel fade>
        <Carousel.Item>
          <img
            className="d-block w-100 mask-cover"
            src={Slide1}
            alt="First slide"
          />
          <Carousel.Caption className="cap-b">
            <h3>Perfect Balance</h3>
            <p>ArchitectUI is like a dream. Some think it's too good to be true! Extensive
            collection of unified React Boostrap Components and Elements.
             </p>
          </Carousel.Caption>
        </Carousel.Item>
        <Carousel.Item>
        <div class="position-relative h-100 d-flex justify-content-center align-items-center bg-premium-dark">
         <div className="slide-img-bg">
         <img
            className="d-block w-100 "
            src={Slide2}
            alt="Second slide"
          />
         </div>
         
          </div>
          <Carousel.Caption className="cap-b">
            <h3>Scalable, Modular, Consistent</h3>
            <p>Easily exclude the components you don't require. Lightweight, consistent
             Bootstrap based styles across all elements and components
            </p>
          </Carousel.Caption>
        </Carousel.Item>
        <Carousel.Item >
          <img
            className="d-block w-100 mask-cover"
            src={Slide3}
            alt="Third slide"
          />


          <Carousel.Caption className="cap-b">
            <h3>Complex, but lightweight</h3>
            <p>We've included a lot of components that cover almost all use cases for any type of application.</p>
          </Carousel.Caption>
        </Carousel.Item>
      </Carousel>
    </div>
  )
}

export default Slider
