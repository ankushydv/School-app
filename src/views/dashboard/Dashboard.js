/* eslint-disable react/jsx-no-comment-textnodes */
// import React, { useState } from "react";
import {
  // CBadge,
  // CButton,
  // CButtonGroup,
  CCard,
  CCardBody,
  // CCardFooter,
  // CCardHeader,
  CCol,
  // CProgress,
  CRow,
  // CCallout,
  CTextarea,
  CLabel,
  CImg,
  // CButton,
} from "@coreui/react";

import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faCalendarAlt } from "@fortawesome/free-solid-svg-icons";
import cardImage from "../../assets/icons/img-1.jpg";


import './dashboard.css'
// import Tables from "../base/tables/Tables";


let options = { month: 'long', weekday: 'short', day: 'numeric' };
let newDate = new Date().toLocaleString([], options)
console.log({ newDate })
let today = new Date();
// let day = today.toLocaleString('default',{day:'short'})
let month = today.toLocaleString('default', { month: 'long' })
console.log(month)
// console.log(day)
console.log({ today });
let date =
  today.getDate() +
  "-" +
  (today.getMonth() + 1) +
  "-" +
  today.getFullYear();

console.log(date);

const Dashboard = () => {

  return (
    <>
      {/* <WidgetsDropdown /> */}
      <CCard>
        <CCardBody>
          <CRow>
            <CCol md="3">
              <CLabel
                htmlFor="textarea-input"
                style={{ fontSize: "30px", fontWeight: "500" }}
              >
                School Wall
              </CLabel>
            </CCol>
          </CRow>
          <CRow>
            <CCol xs="12" md="12">
              <CTextarea
                name="textarea-input"
                id="textarea-input"
                rows="4"
                placeholder="Write here ..."
                style={{ border: "none" }}
              />
            </CCol>
          </CRow>
          <CRow className="mt-4  " style={{ textAlign: "center" }}>
            <CCol md="3" sm="3" className="p1" style={{ color: "#16aaff" }}>
              <svg
                xmlns="http://www.w3.org/2000/svg"
                viewBox="0 0 24 24"
                data-supported-dps="24x24"
                fill="currentColor"
                width="24"
                height="24"
                focusable="false"
              >
                <path d="M21 6h-3l-1.5-3h-9L6 6H3a1 1 0 00-1 1v12a1 1 0 001 1h18a1 1 0 001-1V7a1 1 0 00-1-1zM7.36 8l1.5-3h6.28l1.5 3H20v2h-3.42a5 5 0 00-9.16 0H4V8h3.36zm7.76 4A3.13 3.13 0 1112 8.88 3.13 3.13 0 0115.13 12zM4 18v-7h3.1a5 5 0 109.8 0H20v7H4z"></path>
              </svg>
              <b>Camera</b>
            </CCol>
            <CCol
              md="3"
              sm="3"
              xs="3"
              className="p1"
              style={{ color: "#f7b924" }}
            >
              <svg
                xmlns="http://www.w3.org/2000/svg"
                viewBox="0 0 24 24"
                data-supported-dps="24x24"
                fill="currentColor"
                width="24"
                height="24"
                focusable="false"
              >
                <path d="M21 5.92a1 1 0 00-.57.18L17 8.5V7a1 1 0 00-1-1H3a1 1 0 00-1 1v10a1 1 0 001 1h13a1 1 0 001-1v-1.5l3.43 2.4a1 1 0 00.57.18 1 1 0 001-1V6.92a1 1 0 00-1-1zM15 16H4V8h11v8zm2-5.21l3-2.1v6.62l-3-2.1v-2.42z"></path>
              </svg>
              <b>Video</b>
            </CCol>
            <CCol
              md="3"
              sm="3"
              xs="3"
              className="p1"
              style={{ color: "#3f6ad8" }}
            >
              <svg
                xmlns="http://www.w3.org/2000/svg"
                viewBox="0 0 24 24"
                data-supported-dps="24x24"
                fill="currentColor"
                width="24"
                height="24"
                focusable="false"
              >
                <path d="M21 4H3a1 1 0 00-1 1v14a1 1 0 001 1h18a1 1 0 001-1V5a1 1 0 00-1-1zm-1 14H4V6h16v12zm-7-7H6v5h7v-5zm-1 4H7v-3h5v3zm2-2h4v1h-4v-1zm0 2h4v1h-4v-1zm0-4h4v1h-4v-1zm4-1H6V8h12v2z"></path>
              </svg>
              <b>Article</b>
            </CCol>
            <CCol
              md="3"
              sm="3"
              xs="3"
              className="p1"
              style={{ color: "#3ac47d" }}
            >
              <svg
                version="1.1"
                xmlns="http://www.w3.org/2000/svg"
                fill="currentColor"
                x="0px"
                y="0px"
                viewBox="0 0 219.376 219.376"
                height="20"
              >
                <path d="M127.518,0H40.63c-6.617,0-12,5.383-12,12v195.376c0,6.617,5.383,12,12,12h138.117c6.617,0,12-5.383,12-12V59.227c0-3.204-1.248-6.217-3.513-8.484L135.869,3.382C133.618,1.199,130.66,0,127.518,0zM175.747,204.376H43.63V15h71.767v40.236c0,8.885,7.225,16.114,16.106,16.114h44.244V204.376zM131.503,56.35c-0.609,0-1.105-0.5-1.105-1.114v-31.58l34.968,32.693H131.503zM69.499,88.627h81.5v15h-81.5V88.627z M69.499,113.627h81.5v15h-81.5V113.627z M69.499,138.626h81.5v15h-81.5V138.626zM69.499,163.626h46.5v15h-46.5V163.626z" />
              </svg>
              <b>Document</b>
            </CCol>
          </CRow>
        </CCardBody>
        <CCardBody>
          {/* <CCard> */}
          <CRow>
            <CCol md="6">
              <CCard>
                <CRow style={{ padding: '17px 2px' }}>
                  <CCol md="1" style={{ marginLeft: '1em' }}>
                    <CImg
                      src={"avatars/6.jpg"}
                      className="c-avatar-img-custom"
                      alt="admin@bootstrapmaster.com"
                      width="50px"
                      style={{ borderRadius: "50%" }}
                    />
                  </CCol>
                  <CCol md="10" style={{marginLeft:'1em'}}>
                    <h6 style={{ fontWeight: '700', marginBottom: "0px" }}>Alina Mcloughlin</h6>
                    <div>12+ Years Experience</div>
                    <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s.</p>
                    <img
                      src={cardImage}
                      alt="cover"
                      width="100%"
                      height="150px"
                    />
                    <div style={{ display: 'flex', justifyContent: "space-between" }}>
                      <p style={{ marginTop: "0px" }}>view details</p>
                      <p style={{ marginTop: "0px" }}>
                        <FontAwesomeIcon icon={faCalendarAlt} /> {date}
                      </p>
                    </div>
                  </CCol>
                </CRow>
              </CCard>
            </CCol>
            <CCol md="6">
              <CCard>
                <CRow style={{ padding: '17px 2px' }}>
                  <CCol md="1" style={{ marginLeft: '1em' }}>
                    <CImg
                      src={"avatars/6.jpg"}
                      className="c-avatar-img-custom"
                      alt="admin@bootstrapmaster.com"
                      width="50px"
                      style={{ borderRadius: "50%" }}
                    />
                  </CCol>
                  <CCol md="10" style={{marginLeft:'1em'}}>
                    <h6 style={{ fontWeight: '700', marginBottom: "0px" }}>Alina Mcloughlin</h6>
                    <div>12+ Years Experience</div>
                    <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s.</p>
                    <img
                      src={cardImage}
                      alt="cover"
                      width="100%"
                      height="150px"
                    />
                    <div style={{ display: 'flex', justifyContent: "space-between" }}>
                      <p style={{ marginTop: "0px" }}>view details</p>
                      <p style={{ marginTop: "0px" }}>
                        <FontAwesomeIcon icon={faCalendarAlt} /> {date}
                      </p>
                    </div>
                  </CCol>
                </CRow>
              </CCard>
            </CCol>
          </CRow>
          {/* </CCard>   */}
        </CCardBody>


      </CCard>

      {/* <CCard>
        
          <CCardBody > */}
      {/* <CRow style={{display:'flex', justifyContent:'space-between'}}>
                <CCol md="6" className="ml-auto">
                  <CLabel>My Agenda</CLabel>
                </CCol>
                <CCol md="6" className="custom-btn">
                  <CButton className="seeBtn" >See All</CButton>
                </CCol>
            </CRow> */}
      {/* <CRow >
              <CCol className="newDate">
                <CLabel>{newDate}</CLabel>
              </CCol>
            </CRow> */}
      {/* <CRow > */}
      {/* <table style={{border:'1px solid black', width:'100%'}}>
                <tr style={{border:'1px dotted black'}}>
                  <th style={{width:'10%'}}>12: am</th>
                  <th></th>
                </tr>
                <tr>
                  <td style={{width:'10%'}}>12pm</td>
                  <td>Physic class </td>
                </tr>
                <tr>
                  <td style={{width:'10%'}}>12pm</td>
                  <td>Physic class </td>
                </tr>
                <tr>
                  <td style={{width:'10%'}}>12pm</td>
                  <td>Physic class </td>
                </tr>
              </table> */}
      {/* </CRow>
          </CCardBody> */}
      {/* </CCard> */}


    </>
  );
};

export default Dashboard;
